import { NgModule } from '@angular/core';
import { MatToolbarModule, MatCardModule } from '@angular/material';

const materialModules = [
    MatToolbarModule,
    MatCardModule
]

@NgModule({
    imports: [...materialModules],
    exports: [...materialModules]
})
export class MaterialModule {}
