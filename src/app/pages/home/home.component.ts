import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface PageContents {
  pageTitle: string;
  pageBody: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public pageContents: PageContents = {
    pageTitle: '',
    pageBody: ''
  };

  constructor(private http: HttpClient) { 
    this.http.get('http://localhost:3000/contents').subscribe((resp: PageContents) => {
      console.log(resp);
      this.pageContents = resp;
    });
  }

  ngOnInit() {
  }

}
